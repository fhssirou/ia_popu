# Déscription du projet prédiction de la population
---

|       Mohamed BOUCHIBA       |
------------------------
|       Fodé HISSIROU          |
---


## Résumé 
C'est dans le cadre de notre master MEDAS du CNAM de Paris que nous devons choisir un jeu données et définir un projet.

Aussi notre recherche nous a conduit à utiliser un jeu données public téléchargeable sur [Datagouv](http://www.data.gouv.fr/fr/datasets/donnees-comptables-et-fiscales-des-collectivites-locales/). 

En l'espèce le projet que nous vous présentons cherchera à prédire la population des 36000 communes de France. La population étant l'épicentre de la vie planétaire, par conséquent, ce sera non seulement de celle-ci mais également par rapport à elle que le gouvernement établira et actera toute prise de décision.


### Contexte

Notre démarche d'analyse d'un jeu de données s'inscrit dans le cadre du séminaire d'algorithmique du Master MEDAS. Aussi, après vous avoir exposé le contexte nous vous introduirons le sujet sur lequel portera notre projet puis nous vous présenterons la méthodologie que nous avons choisi d'utiliser en vue d'effectuer notre analyse.
 

Présentation de la DGFIP:

La direction générale des Finances publiques (DGFiP) représente à la fois un service public enraciné dans l’histoire de l’État et une administration nouvelle issue de la fusion en 2008 de la direction générale des impôts et de la direction générale de la comptabilité publique.
 

Ses missions permettent à la fois de contribuer à la solidité financière des institutions publiques et de favoriser un environnement de confiance dans la société, l’économie et les territoires.

## Introduction

Le sujet de notre analyse consiste à établir une prédiction de la population française, laquelle se répartie au sein de 36 000 communes. En effet la population représente un déterminant majeur, et c'est donc fonction de celle-ci que les prises de décision du gouvernement sont établies. 

 
Nous avons sélectionné et téléchargé notre jeu de données sur un site public à savoir celui de [Datagouv](http://www.data.gouv.fr/fr/datasets/donnees-comptables-et-fiscales-des-collectivites-locales/). La base de données du dataset se compose de plusieurs fichiers mais nous avons identifié et conservé qu'un unique fichier afin traiter notre sujet.

### Problématique

En quoi notre jeu de données propose une lecture originale de la prédiction de la population ? Comment permettre aux communes françaises d’anticiper  l’évolution de la population au sein de leur communes sans devoir attendre la publication d’institutions telles que l’INSEE. 

### Sujet

Dans un premier temps l'objectif du projet est de se familiariser avec les méthodes d'analyse de données, la préparation des données et la rédaction d'un code clair et facilement compréhensible par un autre développeur. Dans un second temps l'objectif consiste à analyser ce jeu de données avec R et/ou Python. 

Nous avons choisi de réaliser une prédiction sur (Y) la variable quantitative "Population", qui représente la population par communes, pour ce faire nous avons tout d'abord nettoyé le bruit présent dans le data-set et nous avons ensuite utilisé l'algorithme [LASSO](https://fr.wikipedia.org/wiki/Lasso_(statistiques)) afin de sélectionner nos variables (X) qui vont nous permettre de réaliser notre prédiction

## État de l'art

### Les données

Le data gouv étant une plateforme de sauvegarde de données gouvernemental, permet d'avoir un libre accès et gratuit  

Le dataset est facilement accessible sur le site officielle de datagouv à partir  sur constitue la fiche financière de 36000 communes de la France.

 
### Solutions existants


Selon [le rapport des Nations unies publié mercredi 21 juin](http://www.un.org/apps/newsFr/storyF.asp?NewsID=39703#.Whg0YGs1__i), la population continue d’augmenter et le scénario moyen prévoit que nous serons environ près de 10 milliards en 2050 soit une augmentation de près de 30% par rapport aux 7,55 milliards actuels. 
Cela prouve que chaque année le nombre de la population mondiale augmente de façons exponentielles comme le cas en Afrique alors que dans certains pays d’Europe et d'Asie perd du terrain. 

D'après les études publiées en juin 2017 par [Insee](https://www.insee.fr/fr/statistiques/2867738#graphique-figure1A) prévoit qu'un Français sur deux aura l'âge être dans la vie active. 

![image](/uploads/4e6bf9b256ca3edfc62785a72a93b3b8/image.png)

Leur étude montre que la proportion des personnes âgés de 60 ans ou plus ne cesse d'augmenter. Cela est souvent dû à la baisse de la fécondité dont connaît l’Europe tout entier. D'après John Wilmoth "quand la fertilité tombe au-dessous du seuil de 2,1 naissances par femme, le nombre de bébés chaque année est insuffisant pour remplacer la génération des parents"

La population est un groupe d’individus partageant un caractère commun comme par exemple le lieu d’habitation. C'est dans ce cadre que le chercheur du nom de [Hervé LE BRAS au C.N.R.S](http://www.universalis-edu.com/encyclopedie/demographie/) a fait une étude sur la question. IL prouve que la dermographie d'un pays ne se limite pas seulement au taux de natalité ou de mortalité de ce pays, mais qu'elle va bien au-delà. 


Il est donc important pour chaque pays de mesurer l’évolution des populations pour prendre certaines décisions budgétaires dans le cadre économique. Le recensement de la population a d’énorme d'avantages, il permet de connaître le taux de chômages, de faire le calcul de la dotation budgétaire aux communes, d’étudier aussi certaines questions comme immigrations. En effet, des dizaines de milliers de personnes immigrent chaque année pour changer la localité qui est souvent due pour diverses raisons (réfugier de guerre, des immigrations dues à des facteurs économiques ... ). Il assemble de ces facteurs font que plusieurs pays ont réellement besoin de connaître leur dermographie. 


[A voir aussi](https://www.insee.fr/fr/statistiques/1281151)


### Études algorithmiques
---

## Réalisations 

### Préparation des données

### Implémentation de l’algorithme

#### A

#### B

#### C

### Solutions

## Conclusion


## Références 
Hervé LE BRAS, « DÉMOGRAPHIE », Encyclopædia Universalis [en ligne], consulté le 21 novembre 2017. URL : http://www.universalis-edu.com/encyclopedie/demographie/
