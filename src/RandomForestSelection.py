import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import csv
import seaborn as sns
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import scale
from sklearn import model_selection
from sklearn.linear_model import LinearRegression
from sklearn.neighbors import KNeighborsRegressor
from sklearn import preprocessing
from sklearn.linear_model import LassoLarsCV
from sklearn.metrics import r2_score
from sklearn.ensemble import RandomForestRegressor
from sklearn.datasets import make_regression
from sklearn.multioutput import MultiOutputRegressor
from sklearn.datasets import make_classification
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.svm import SVR
import tensorflow as tf
from tensorflow.contrib.tensor_forest.python import tensor_forest
# Ignore all GPUs, tf random forest does not benefit from it.
import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'



# print(parametre_refactor)
def loadFile():
    df = pd.read_table("../dataset/city_echantillon.csv",low_memory=False, error_bad_lines=False, sep=',', header=0)
    file = open("../dataset/rename_param.csv","r")

    parametre_refactor_csv = csv.reader(file)
    list_param_rename = []

    for row in parametre_refactor_csv:
        list_param_rename.append(row[1])
    df.columns = list_param_rename
    return df

def delColumnsNotValues(df):
    df= df.drop(["url", "cog", "type_zone_administrative"], axis=1)
    len_row= df.shape[0]
    for col in df.columns:
        verif = (len_row - df[col].isnull().sum()) / len_row
        if verif <= 0.80 :
            df = df.drop(col, 1)

    return df

def correlation(dfs):

    df_num = dfs[df.columns]
    df_num = df_num.drop("nom", 1)
    corr = df_num.astype(float).corr()
    mask = np.zeros_like(corr, dtype=np.bool)
    mask[np.triu_indices_from(mask)] = True

    f, ax = plt.subplots(figsize=(11, 9))
    cmap = sns.diverging_palette(220, 10, as_cmap=True)
    sns.heatmap(corr,mask=mask, linewidths=.5, vmax=0.3, center=0, square=True,cmap=cmap, cbar_kws={"shrink": .5})
    plt.show()

def splitByYear(dfs):
    dfs_dict={}
    grouped = dfs.groupby("annee")
    for annee, group in grouped:

        dfs_dict[annee]=group.drop("annee", 1)

    return dfs_dict

def splitOneDataframe(df):

    df["split"] = ["Valid" if np.random.random_sample() >= 0.94 else "Test" if np.random.random_sample() >= 0.75 else "Train" for i in range(len(df)) ]

    train, test, valid = [df[df['split'] == "Train"], df[df['split'] == "Test"], df[df['split'] == "Valid"]]
    train = train.drop('split', 1)
    test = test.drop('split', 1)
    valid = valid.drop('split', 1)
    df = df.drop('split', 1)

    return {"Train": train, "Test": test, "Valid": valid}


def splitAllDataframe(dfs_dict):
    dfs_split_dict={}
    for key, values in dfs_dict.items():
        dfs_split_dict[key] = splitOneDataframe(values)

    for key, values in dfs_split_dict.items():
        print(key, " --> ", len(values["Train"]), " --- ", len(values["Test"])," --- ", len(values["Valid"]))
    return dfs_split_dict


def getXandY(dfYear):
    dfYear = dfYear[dfYear.nom != "PARIS"]
    Y = dfYear.population.values
    taget_names = dfYear.nom.values
    dfYear = dfYear.drop(['nom','population'], axis=1)
    X = dfYear.values
    data = X
    # data = preprocessing.normalize(X)
    dicoYearXandY = {"target": Y, "data": data, "target_names":taget_names}
    return dicoYearXandY



def getXandY_allYear(dfs_split_dict):
    for key, values in dfs_split_dict.items():
        test = values["Test"]
        train = values["Train"]
        valid = values["Valid"]

        dico_test = getXandY(test)
        test_X = dico_test["data"]
        test_y = dico_test["target"]
        target_names_test = dico_test["target_names"]
        Test = {'X': test_X, 'y': test_y, 'target_names':target_names_test}

        dico_train = getXandY(train)
        train_X = dico_train["data"]
        train_y = dico_train["target"]
        target_names_train = dico_train["target_names"]
        Train = {'X': train_X, 'y': train_y, 'target_names':target_names_train}

        dico_valid = getXandY(valid)
        valid_X = dico_valid["data"]
        valid_y = dico_valid["target"]
        target_names_valid = dico_valid["target_names"]
        Valid = {'X': valid_X, 'y': valid_y, 'target_names':target_names_valid}


        dfs_split_dict[key] = {'Test': Test, 'Train': Train, 'Validation': Valid}

    return dfs_split_dict

df = loadFile()
df = delColumnsNotValues(df)
# df.describe()
# correlation(df)
dicDfYear = splitByYear(df)
dfs_split_dict = splitAllDataframe(dicDfYear)
Big_dico = getXandY_allYear(dfs_split_dict)


######################################### Netoyage Terminer ##################################

Test = Big_dico[2000]["Test"]
Train = Big_dico[2000]["Train"]
Validation = Big_dico[2000]["Validation"]

train_X = Train["X"]
train_y = Train["y"]

test_X = Test["X"]
test_y = Test["y"]
test_name = Test["target_names"]

valid_X = Validation["X"]
valid_y = Validation["y"]

####################################### Selection de variable ###################################

# model_lassocv=LassoLarsCV(precompute=False, n_jobs=2, cv=6).fit(train_X,train_y)
# predictorsNames = dicDfYear[2000].columns
# predictorsNames = predictorsNames.drop(["nom","population", "split"])
#
# coef = pd.Series(model_lassocv.coef_, index = predictorsNames)
# imp_coef = pd.concat([coef.sort_values().head(10),coef.sort_values().tail(10)])
# matplotlib.rcParams['figure.figsize'] = (14.0, 8.0)
# imp_coef.plot(kind = "bar")
# plt.title("Weight Coefficients in the Lasso Model")
#
# plt.show()


####################################### Random Forest #############################################



# Annee = Big_dico[2000]
# Test = Annee["Test"]
# print(Test["target_names"][3])
# print(Test["X"][3])
# print(Test["y"][3])



regr = RandomForestRegressor(max_depth=10000, random_state=0, n_jobs=3)
regr.fit(train_X, train_y)
# print(regr.feature_importances_)

print(regr.decision_path(test_X))
print("Score Test : "+str(regr.score(test_X,test_y)))
print("Score Validation : "+str(regr.score(valid_X,valid_y)))





# clf = SVR()
# clf.fit(train_X, train_y)
# print("Score Test : "+str(clf.score(test_X,test_y)))
# print("Score Validation : "+str(clf.score(valid_X,valid_y)))


X = test_X
y = test_y


# Build a forest and compute the feature importances
forest = ExtraTreesClassifier(n_estimators=2,
                              random_state=0)

forest.fit(X, y)
importances = forest.feature_importances_
std = np.std([tree.feature_importances_ for tree in forest.estimators_],
             axis=0)
indices = np.argsort(importances)[::-1]

# Print the feature ranking
print("Feature ranking:")

for f in range(X.shape[1]):
    print("%d. feature %d (%f)" % (f + 1, indices[f], importances[indices[f]]))

# Plot the feature importances of the forest
plt.figure()
plt.title("Feature importances")
plt.bar(range(X.shape[1]), importances[indices],
       color="r", yerr=std[indices], align="center")
plt.xticks(range(X.shape[1]), indices)
plt.xlim([-1, X.shape[1]])
plt.show()


annee = dicDfYear[2000]
# annee = annee.drop(annee.columns[[2, 13, 16,8]], axis=1)
# annee = annee.drop(["split"], axis=1)
# annne = splitOneDataframe(annee)

# print(test_X[3])
np.delete(train_X, [0,34,5,38,14], axis=1)
np.delete(test_X, [0,34,5,38,14], axis=1)
np.delete(test_X, [0,34,5,38,14], axis=1)

annee = annee.drop(['nom','population'], axis=1)
print(annee.columns[[0,34,5,38,14]])




regr = RandomForestRegressor(max_depth=10, random_state=0, n_jobs=3)
regr.fit(train_X, train_y)
# print(regr.feature_importances_)

print(regr.decision_path(test_X))
print("Score Test : "+str(regr.score(test_X,test_y)))
print("Score Validation : "+str(regr.score(valid_X,valid_y)))
