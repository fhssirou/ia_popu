
# coding: utf-8

# # Déscription du projet prédiction de la population
# ---
# 
# |       Mohamed BOUCHIBA       |
# ------------------------
# |       Fodé HISSIROU          |
# ------------------------
# |       Mohamed BOUSSAA        |
# ---
# 
# 
# ## Résumé 
# C'est dans le cadre de notre master MEDAS du CNAM de Paris que nous devons choisir un jeu données et définir un projet.
# 
# Aussi notre recherche nous a conduit à utiliser un jeu données public téléchargeable sur [Datagouv](http://www.data.gouv.fr/fr/datasets/donnees-comptables-et-fiscales-des-collectivites-locales/). 
# 
# En l'espèce le projet que nous vous présentons cherchera à prédire la population des 36000 communes de France. La population étant l'épicentre de la vie planétaire, par conséquent, ce sera non seulement de celle-ci mais également par rapport à elle que le gouvernement établira et actera toute prise de décision.
# 
# 
# ### Contexte
# 
# Notre démarche d'analyse d'un jeu de données s'inscrit dans le cadre du séminaire d'algorithmique du Master MEDAS. Aussi, après vous avoir exposé le contexte nous vous introduirons le sujet sur lequel portera notre projet puis nous vous présenterons la méthodologie que nous avons choisi d'utiliser en vue d'effectuer notre analyse.
#  
# 
# Présentation de la DGFIP:
# 
# La direction générale des Finances publiques (DGFiP) représente à la fois un service public enraciné dans l’histoire de l’État et une administration nouvelle issue de la fusion en 2008 de la direction générale des impôts et de la direction générale de la comptabilité publique.
#  
# 
# Ses missions permettent à la fois de contribuer à la solidité financière des institutions publiques et de favoriser un environnement de confiance dans la société, l’économie et les territoires.
# 
# ## Introduction
# 
# Le sujet de notre analyse consiste à établir une prédiction de la population française, laquelle se répartie au sein de 36 000 communes. En effet la population représente un déterminant majeur, et c'est donc fonction de celle-ci que les prises de décision du gouvernement sont établies. 
# 
#  
# Nous avons sélectionné et téléchargé notre jeu de données sur un site public à savoir celui de [Datagouv](http://www.data.gouv.fr/fr/datasets/donnees-comptables-et-fiscales-des-collectivites-locales/). La base de données du dataset se compose de plusieurs fichiers mais nous avons identifié et conservé qu'un unique fichier afin traiter notre sujet.
# 
# ### Problématique
# 
# En quoi notre jeu de données propose une lecture originale de la prédiction de la population ? Comment permettre aux communes françaises d’anticiper  l’évolution de la population au sein de leur communes sans devoir attendre la publication d’institutions telles que l’INSEE. 
# 
# ### Sujet
# 
# Dans un premier temps l'objectif du projet est de se familiariser avec les méthodes d'analyse de données, la préparation des données et la rédaction d'un code clair et facilement compréhensible par un autre développeur. Dans un second temps l'objectif consiste à analyser ce jeu de données avec R et/ou Python. 
# 
# Nous avons choisi de réaliser une prédiction sur (Y) la variable quantitative "Population", qui représente la population par communes, pour ce faire nous avons tout d'abord nettoyé le bruit présent dans le data-set et nous avons ensuite utilisé l'algorithme [LASSO](https://fr.wikipedia.org/wiki/Lasso_(statistiques)) afin de sélectionner nos variables (X) qui vont nous permettre de réaliser notre prédiction
# 
# ## État de l'art
# 
# ### Les données
# 
# Le data gouv étant une plateforme de sauvegarde de données gouvernemental, permet d'avoir un libre accès et gratuit  
# 
# Le dataset est facilement accessible sur le site officielle de datagouv à partir  sur constitue la fiche financière de 36000 communes de la France.
# 
#  
# ### Solutions existants
# 
# 
# Selon [le rapport des Nations unies publié mercredi 21 juin](http://www.un.org/apps/newsFr/storyF.asp?NewsID=39703#.Whg0YGs1__i), la population continue d’augmenter et le scénario moyen prévoit que nous serons environ près de 10 milliards en 2050 soit une augmentation de près de 30% par rapport aux 7,55 milliards actuels. 
# Cela prouve que chaque année le nombre de la population mondiale augmente de façons exponentielles comme le cas en Afrique alors que dans certains pays d’Europe et d'Asie perd du terrain. 
# 
# D'après les études publiées en juin 2017 par [Insee](https://www.insee.fr/fr/statistiques/2867738#graphique-figure1A) prévoit qu'un Français sur deux aura l'âge être dans la vie active. 
# 
# ![image](/uploads/4e6bf9b256ca3edfc62785a72a93b3b8/image.png)
# 
# Leur étude montre que la proportion des personnes âgés de 60 ans ou plus ne cesse d'augmenter. Cela est souvent dû à la baisse de la fécondité dont connaît l’Europe tout entier. D'après John Wilmoth "quand la fertilité tombe au-dessous du seuil de 2,1 naissances par femme, le nombre de bébés chaque année est insuffisant pour remplacer la génération des parents"
# 
# La population est un groupe d’individus partageant un caractère commun comme par exemple le lieu d’habitation. C'est dans ce cadre que le chercheur du nom de [Hervé LE BRAS au C.N.R.S](http://www.universalis-edu.com/encyclopedie/demographie/) a fait une étude sur la question. IL prouve que la dermographie d'un pays ne se limite pas seulement au taux de natalité ou de mortalité de ce pays, mais qu'elle va bien au-delà. 
# 
# 
# Il est donc important pour chaque pays de mesurer l’évolution des populations pour prendre certaines décisions budgétaires dans le cadre économique. Le recensement de la population a d’énorme d'avantages, il permet de connaître le taux de chômages, de faire le calcul de la dotation budgétaire aux communes, d’étudier aussi certaines questions comme immigrations. En effet, des dizaines de milliers de personnes immigrent chaque année pour changer la localité qui est souvent due pour diverses raisons (réfugier de guerre, des immigrations dues à des facteurs économiques ... ). Il assemble de ces facteurs font que plusieurs pays ont réellement besoin de connaître leur dermographie. 
# 
# 
# [A voir aussi](https://www.insee.fr/fr/statistiques/1281151)
# 
# 
# ### Études algorithmiques
# ---
# 
# ## Réalisations 
# 
# ### Préparation des données
# 
# ### Implémentation de l’algorithme
# 
# #### A
# 
# #### B
# 
# #### C
# 
# ### Solutions
# 
# ## Conclusion
# 
# 
# ## Références 
# Hervé LE BRAS, « DÉMOGRAPHIE », Encyclopædia Universalis [en ligne], consulté le 21 novembre 2017. URL : http://www.universalis-edu.com/encyclopedie/demographie/
# 

# In[35]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import csv
import seaborn as sns
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import scale
from sklearn import model_selection
from sklearn.linear_model import LinearRegression
from sklearn.neighbors import KNeighborsRegressor
from sklearn.svm import SVR
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.ensemble import RandomForestRegressor

from sklearn.linear_model import LassoLarsCV
from sklearn.metrics import roc_auc_score
from sklearn.metrics import r2_score
from sklearn.metrics import roc_curve


# # Chargement des données
# Cette fonction permet de charger deux fichiers CSV (le dataset et les labels du dataset). Elle remplace les noms des colonnes du dataset par celles du fichier renam_param.csv.
# Suite à des problèmes de matériel, nous avons choisi d'utiliser un échantillon de données de notre dataset goblal. L'échantillon ci-dessous représente les données sur les 36000 communes de la France de 2000 à 2001

# In[36]:


# print(parametre_refactor)
def loadFile():
    df = pd.read_table("../dataset/city_echantillon.csv",low_memory=False, error_bad_lines=False, sep=',', header=0)
    file = open("../dataset/rename_param.csv","r")

    parametre_refactor_csv = csv.reader(file)
    list_param_rename = []

    for row in parametre_refactor_csv:
        list_param_rename.append(row[1])
    df.columns = list_param_rename
    #df = df.drop(df.columns[[1, 5]], axis=1)
    return df

df = loadFile()

df


# La ligne dessous nous indique la taille de notre dataset qui est de 1099929 lignes et 74 colonnes.

# In[37]:


df.shape


# # Netoyage des données
# 
# En analysant le dataset, on s'est rendu compte qu'il contient beaucoup de colonnes vides(aucune valeur). Or certaines colonnes sont à moitié vides jusqu'à 80%, donc nous avons choisi de supprimer ces élements. Nous supprimons aussi certaines colonnes du type string, que nous jugeons unitiles comme les urls, les codes postables.
# Cette fonction delcolumnsnotvalues permet de supprimer les colonnes vides. Elle prend en paramètre le dataset et retourne le dataset sans les columns vides. On peut voir ci-dessous le nombre de colonnes qui est passé de 74 à 43. Donc nous avons 45 colonnes.

# In[38]:


def delColumnsNotValues(df):
    #del_list=["url", "cog", "type_zone_administrative"]
    df= df.drop(["url", "cog", "type_zone_administrative"], axis=1)
    len_row= df.shape[0]
    for col in df.columns:
        verif = (len_row - df[col].isnull().sum()) / len_row
        if verif <= 0.80 :
            df = df.drop(col, 1)

    return df

df = delColumnsNotValues(df)

df.shape


# La méthode describe permet d'afficher un résumé des statistiques de les données numériques (somme, moyenne, min, max, quartiles, etc.)

# In[39]:


df.describe()


# In[40]:


#plt.figure(figsize=(14,14))
#sns.pairplot(df,diag_kind="kde")


# # Correlation
# 
# Rappelons qu’une matrice de corrélation est utilisée pour évaluer la dépendance entre plusieurs variables en même temps. Le résultat est une table contenant les coefficients de corrélation entre chaque variable et les autres.
# 
# Notre analyse nous livre sa matrice de corrélation qui confirme l’aspect similaire des variables exprimant la population avec les variables du jeu de données. Elle montre une corrélation plus forte que les autres variables car elle est corrélée avec plus de variable que toutes les autres, mais aussi car elle possède une corrélation.
# Pour identifier les corrélations de notre matrice nous avons choisi d’utiliser une palette de couleurs et non des données numériques par soucis de lisibilité. En effet, le nombre de caractères présents sur chaque champ est si grand qu’une comparaison ne être faite sans difficulté d’analyse.
# 
# Les corrélations positives sont affichées en rouge et les corrélations négatives en bleu. L’intensité de la couleur est proportionnelle aux coefficients de corrélation. A droite de la matrice, la légende de couleurs montre les coefficients de corrélation et les couleurs correspondantes.
# 
# Comme indiqué dans la légende, les coefficients de corrélation entre 0 et 0.3 sont remplacés par une couleur qui tend du blanc au rouge dans un dégradé de plus en plus foncé; les coefficients de corrélation entre 0 et -0.3 sont remplacés par une couleur qui tend du du blanc au bleu dans un dégradée de plus en plus foncé.

# In[41]:


def correlation(dfs):

    df_num = dfs[df.columns]
    df_num = df_num.drop("nom", 1)
    corr = df_num.astype(float).corr()
    mask = np.zeros_like(corr, dtype=np.bool)
    mask[np.triu_indices_from(mask)] = True

    f, ax = plt.subplots(figsize=(11, 9))
    cmap = sns.diverging_palette(220, 10, as_cmap=True)
    sns.heatmap(corr,mask=mask, linewidths=.5, vmax=0.3, center=0, square=True,cmap=cmap, cbar_kws={"shrink": .5})
    plt.show()


correlation(df)


# # Séparation du jeu de donée
# 
# Nous avons décidé de séparer manuellement le data-set au lieu d'utiliser la méthode sur sklearn. Après une étude de la fonction implémentée dans la librairie sklearn nous avons décidé de passer par l'aléatoire pour le split du data set. Pour répondre à notre problématique nous avons dû découper le data set par année. Chaque année et ensuite découper par 3, la partie Train, la partie teste et la partie Valiation, la partie validation est une sorte de données fictives même si elles sont bien réelles. Train = 75%, Test = 18%, Validation = 7%.

# In[42]:


def splitByYear(dfs):
    dfs_dict={}
    grouped = dfs.groupby("annee")
    for annee, group in grouped:

        dfs_dict[annee]=group.drop("annee", 1)

    return dfs_dict

dicDfYear = splitByYear(df)

def splitOneDataframe(df):

    df["split"] = ["Valid" if np.random.random_sample() >= 0.94 else "Test" if np.random.random_sample() >= 0.75 else "Train" for i in range(len(df)) ]

    train, test, valid = df.loc[df['split'] == "Train"], df.loc[df['split'] == "Test"], df.loc[df['split'] == "Valid"]
    train = train.drop('split', 1)
    test = test.drop('split', 1)
    valid = valid.drop('split', 1)
    df = df.drop('split', 1)

    return {"Train": train, "Test": test, "Valid": valid}


def splitAllDataframe(dfs_dict):
    dfs_split_dict={}
    for key, values in dfs_dict.items():
        dfs_split_dict[key] = splitOneDataframe(values)

    print(len(dfs_split_dict))
    for key, values in dfs_split_dict.items():
        print(key, " --> ", len(values["Train"]), " --- ", len(values["Test"])," --- ", len(values["Valid"]))
    return dfs_split_dict



dfs_split_dict = splitAllDataframe(dicDfYear)

#dico_year = getXandY(dicDfYear[2000])


# # Génration du X et du y
# 
# Dans cette partie nous sélectionnons les X et les y ainsi que les noms des villes (target_names) de chaque partie (Train, Test, Validation) de chaque année. Nous avons donc à ce stade du traitement des donnée "propre" nous n'avons pas eu le besoin de factoriser une colonne, nos X sont donc sous forme de matrice et notre y sous forme de vecteur. 
# 
# Il sont donc utilisable pour la prédiction.

# In[43]:


def getXandY(dfYear):
    if ('nom' in dfYear.columns) and ('population' in dfYear.columns) :
        Y = dfYear["population"]
        taget_names = dfYear["nom"]
        data = dfYear.drop(['nom','population'], axis=1)
        #data = dfYear.values
        #data = scale(dfYear.values.astype('float64'))
        dicoYearXandY = {"target": Y, "data": data, "target_names":taget_names}
        return dicoYearXandY
    return dfYear

def getXandY_allYear(dfs_split_dict):
    for key, values in dfs_split_dict.items():
        test = values["Test"]
        train = values["Train"]
        valid = values["Valid"]
        
        dico_test = getXandY(test)
        test_X = dico_test["data"]
        test_y = dico_test["target"]
        Test = {'X': test_X, 'y': test_y}

        dico_train = getXandY(train)
        train_X = dico_train["data"]
        train_y = dico_train["target"]
        Train = {'X': train_X, 'y': train_y}

        dico_valid = getXandY(valid)
        valid_X = dico_valid["data"]
        valid_y = dico_valid["target"]
        Valid = {'X': valid_X, 'y': valid_y}
        dfs_split_dict[key] = {'Test': Test, 'Train': Train, 'Validation': Valid}
    
    return dfs_split_dict 

Big_dico = getXandY_allYear(dfs_split_dict)


# In[44]:



Big_dico


# Le lasso permet de supprimer des variables en mettant leur poids à zéro. C'est le cas si deux variables sont corrélées. L'une sera sélectionnée par le lasso, l'autre supprimée. La fonction get best permet de sélectionner les noms des variables sélectionnées par le lasso. Par contre la fonction ModelLassoYear permet d'exécuter le lasso et aussi de faire des subplots. Elle fonctionne uniquement pour une année. Donc on utilise une autre fonction pour généraliser pour toutes les années

# In[45]:


Index_of_bestfeatures ={}
def get_best(ser):
    list_pos=[]
    pos=0
    for index, val in ser.items():
        if val > 0:
            list_pos.append(index)
        pos+=1
    return list_pos
    
def modelLassoOneYear(train, test, validation, predictorsNames, key, ):
    
    model_lassocv=LassoLarsCV(precompute=False, n_jobs=2).fit( train["X"],train["y"])
    

    coef = pd.Series(model_lassocv.coef_, index = predictorsNames)

    Index_of_bestfeatures[key] = get_best(coef)
    
    imp_coef = pd.concat([coef.sort_values().head(10),coef.sort_values().tail(10)])

    fig = plt.figure()
    plt.ylabel(str(key), fontsize=20)
        
    fig.add_subplot(131)
    matplotlib.rcParams['figure.figsize'] = (14.0, 8.0)  
    imp_coef.plot(kind = "bar")
    plt.title("Weight Coefficients in the Lasso Model")

    fig.add_subplot(132)
    list_alphas = -np.log10(model_lassocv.alphas_)
    ax = plt.gca()
    plt.plot(list_alphas, model_lassocv.coef_path_.T)
    plt.axvline(-np.log10(model_lassocv.alpha_), linestyle='--', color='r',label='alpha CV')
    plt.ylabel('Coef de Regression')
    plt.xlabel('-log(alpha)')
    plt.title("Coef de Regression Lasso")

    fig.add_subplot(133)
    list_alphascv = -np.log10(model_lassocv.cv_alphas_)
    plt.plot(list_alphascv, model_lassocv.cv_mse_path_, ':')
    plt.plot(list_alphascv, model_lassocv.cv_mse_path_.mean(axis=-1), 'k',label='MSE', linewidth=2)
    plt.axvline(-np.log10(model_lassocv.alpha_), linestyle='--', color='r',label='alpha CV')
    plt.legend()
    plt.xlabel('-log(alpha)')
    plt.ylabel('MSE')
    plt.title('Mean squared error (MSE)')

    return fig



# In[46]:


def lassoAllYear(df_big_dico):
    
    predictorsNames = dicDfYear[2000].columns 
    predictorsNames = predictorsNames.drop(["nom","population", "split"])
    
    index=1
    figure = plt.figure()
    for key in df_big_dico.keys():
        Test = df_big_dico[key]["Test"]
        Train = df_big_dico[key]["Train"]
        Validation = df_big_dico[key]["Validation"]

        figure.add_subplot(index,1,1)
        modelLassoOneYear(Train, Test, Validation, predictorsNames, key) 
        
        index+=1

    plt.show()
lassoAllYear(Big_dico)


# In[47]:



Index_of_bestfeatures


# # Sklearn fonction
# 
# La fonction allmodelregresion nous permet de faire appel aux fonctions de prédiction qui se trouve dans sklearn telle que KNN ou RandomForest qui prennent en paramètre des X et un y pour fair un "fit" et une fois le modèle entrainer il peut effectuer des prédictions en l'occurrence ici sur une variable quantitative qui est la population. Nous avons essayé d'utiliser TensorFlow mais nous avons manqué de puissance de calcul même en utilisant CUDA pour avoir assez à notre GPU cela nous à pas suffi.

# In[ ]:





def allModelRegression(train, test, validation):

    LRModel = LinearRegression()
    KNNModel = KNeighborsRegressor(n_neighbors=50)
    SVRModel = SVR(kernel='rbf', C=1e3, gamma=0.1)
    GBModel = GradientBoostingRegressor(min_samples_leaf= 4, learning_rate= 0.1, max_depth= 10)
    RFModel = RandomForestRegressor(max_depth=10, random_state=0, n_jobs=3, )
    
    LRModel.fit(train["X"], train["y"])
    KNNModel.fit(train["X"], train["y"])
    SVRModel.fit(train["X"], train["y"])
    GBModel.fit(train["X"], train["y"])
    RFModel.fit(train["X"], train["y"])
    
    
    #lr_y_pred_test  =  LRModel.predict(Test["X"])
    #knn_y_pred_test =  KNNModel.predict(Test["X"])
    #svr_y_pred_test =  SVRModel.predict(Test["X"])
    #GB_y_prd_test   =  GBModel.predict(Test["X"])
    
    print("Linear Reg Model Test score: " , LRModel.score(test["X"], test["y"]))
    print("KNN Reg Test score: ", KNNModel.score(test["X"], test["y"]))
    print("SVR Reg Test score: ", SVRModel.score(test["X"], test["y"]))
    print("GBoosting Reg Test score: ", GBModel.score(test["X"], test["y"]))
    print("RandForest Reg Test score: ", RFModel.score(test["X"], test["y"]))
        
    print("LRModel validation score: " , LRModel.score(validation["X"], validation["y"]))
    print("KNN Reg validation score: ", KNNModel.score(validation["X"], validation["y"]))
    print("SVRModel validation score: ", SVRModel.score(validation["X"], validation["y"]))
    print("GBoModel validation score: ", GBModel.score(validation["X"], validation["y"]))
    print("RandForest Reg validation score: ", RFModel.score(validation["X"], validation["y"]))
        


# # Prédiction
# 
# La fonction RunModelAllYear est la fonction de fin qui permet de faire appel à toutes les algo de prédiction et de nous permet d'avoir dès une vue d'ensemble sur la performance de tel ou tel algo. Nos données fictives ici sont la partie validation.

# In[ ]:


def runModelAllYear(df_big_dico):
    
    predictorsNames = dicDfYear[2000].columns 
    predictorsNames = predictorsNames.drop(["nom","population", "split"])

    for key in df_big_dico.keys():
        print("\n------------", key,"--------------")
                
        Test = df_big_dico[key]["Test"]
        Train = df_big_dico[key]["Train"]
        Validation = df_big_dico[key]["Validation"]
        
        Test["X"] = Test["X"][Index_of_bestfeatures[key]]
        Train["X"] = Train["X"][Index_of_bestfeatures[key]]
        Validation["X"] = Validation["X"][Index_of_bestfeatures[key]]
        
        allModelRegression(Train, Test, Validation)
        
runModelAllYear(Big_dico)


# In[ ]:


Nous avons donc pu répondre à notre problématique.

